package ru.pyshinskiy.tm.command.user;

import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.role.Role;

public final class UserCreateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user_create";
    }

    @Override
    public String description() {
        return "create new user";
    }

    @Override
    public boolean isAllowed(User user) {
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER CREATE]");
        System.out.println("ENTER USERNAME");
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(terminalService.nextLine());
        System.out.println("ENTER PASSWORD");
        user.setPassword(terminalService.nextLine());
        serviceLocator.getUserService().persist(user);
        System.out.println("[OK]");
    }
}
