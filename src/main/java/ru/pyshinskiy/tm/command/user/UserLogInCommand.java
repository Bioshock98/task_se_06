package ru.pyshinskiy.tm.command.user;

import org.apache.commons.codec.digest.DigestUtils;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printUser;

public final class UserLogInCommand extends AbstractCommand {
    
    @Override
    public boolean isAllowed(final User user) {
        return true;
    }

    @Override
    public String command() {
        return "user_log_in";
    }

    @Override
    public String description() {
        return "log in";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOG IN]");
        System.out.println("ENTER LOGIN");
        String login = terminalService.nextLine();
        while(!checkLogin(login)) {
            System.out.println("Login doesn't exist");
            System.out.println("ENTER LOGIN");
            login = terminalService.nextLine();
        }
        System.out.println("[OK]");
        User user = null;
        for(User user1 : serviceLocator.getUserService().findAll()) {
            if(user1.getLogin().equals(login)) user = user1;
        }
        printUser(user);
        System.out.println("ENTER PASSWORD");
        String password = terminalService.nextLine();
        while(!user.getPassword().equals(DigestUtils.md5Hex(password))) {
            System.out.println("Invalid password");
            System.out.println("ENTER PASSWORD");
            password = terminalService.nextLine();
        }
        serviceLocator.setCurrentUser(user);
        System.out.println("[OK]");
    }

    private boolean checkLogin(final String login) {
        for(final User user : serviceLocator.getUserService().findAll()) {
            if(user.getLogin().equals(login)) return true;
        }
        return false;
    }
}
