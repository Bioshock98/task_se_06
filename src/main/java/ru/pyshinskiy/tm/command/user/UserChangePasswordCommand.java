package ru.pyshinskiy.tm.command.user;

import org.apache.commons.codec.digest.DigestUtils;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;

public final class UserChangePasswordCommand extends AbstractCommand {

    @Override
    public String command() {
        return "user_change_password";
    }

    @Override
    public String description() {
        return "change user password";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER CHANGE PASSWORD");
        System.out.println("ENTER OLD PASSWORD");
        final String userPassword = serviceLocator.getCurrentUser().getPassword();
        while(!userPassword.equals(DigestUtils.md5Hex(terminalService.nextLine()))) {
            System.out.println("Incorrect password");
        }
        System.out.println("ENTER NEW PASSWORD");
        final User user = new User();
        user.setId(serviceLocator.getCurrentUser().getId());
        user.setPassword(terminalService.nextLine());
        serviceLocator.getUserService().merge(user);
        System.out.println("[OK]");
    }
}
