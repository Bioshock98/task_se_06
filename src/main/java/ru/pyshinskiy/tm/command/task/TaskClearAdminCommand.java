package ru.pyshinskiy.tm.command.task;

import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.role.Role;

public final class TaskClearAdminCommand extends AbstractCommand {

    @Override
    public boolean isAllowed(final User user) {
        if(user == null) return false;
        return Role.ADMINISTRATOR.equals(user.getRole());
    }

    @Override
    public String command() {
        return "task_clear_admin";
    }

    @Override
    public String description() {
        return "clear all users tasks";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getTaskService().removeAll();
    }
}
