package ru.pyshinskiy.tm.command.task;

import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.entity.User;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskUnattachCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task_unattach";
    }

    @Override
    public String description() {
        return "unattach task from project";
    }

    @Override
    public void execute() throws Exception {
        final ITaskService taskService = serviceLocator.getTaskService();
        final User currentUser = serviceLocator.getCurrentUser();
        System.out.println("[UNATTACH TASK]");
        printTasks(taskService.findAll(currentUser.getId()));
        System.out.println("ENTER TASK ID");
        final String taskId = taskService.getIdByNumber(Integer.parseInt(terminalService.nextLine()) - 1);
        final Task task = taskService.findOne(currentUser.getId(), taskId);
        task.setProjectId(null);
        taskService.merge(task);
        System.out.println("[OK]");
    }
}
