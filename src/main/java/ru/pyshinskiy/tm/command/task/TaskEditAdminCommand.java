package ru.pyshinskiy.tm.command.task;

import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.role.Role;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateFromString;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskEditAdminCommand extends AbstractCommand {
    
    @Override
    public boolean isAllowed(final User user) {
        if(user == null) return false;
        return Role.ADMINISTRATOR.equals(user.getRole());
    }

    @Override
    public String command() {
        return "task_edit_admin";
    }

    @Override
    public String description() {
        return "edit any task";
    }

    @Override
    public void execute() throws Exception {
        final ITaskService taskService = serviceLocator.getTaskService();
        System.out.println("[TASK EDIT]");
        System.out.println("ENTER TASK ID");
        printTasks(taskService.findAll());
        final String taskId = taskService.getIdByNumber(Integer.parseInt(terminalService.nextLine()) - 1);
        final Task task = taskService.findOne(taskId);
        System.out.println("[ENTER TASK NAME]");
        final Task anotherTask = new Task(task.getUserId());
        anotherTask.setName(terminalService.nextLine());
        anotherTask.setId(task.getId());
        System.out.println("ENTER TASK DESCRIPTION");
        anotherTask.setDescription(terminalService.nextLine());
        System.out.println("ENTER START DATE");
        anotherTask.setStartDate(parseDateFromString(terminalService.nextLine()));
        System.out.println("ENTER END DATE");
        anotherTask.setEndDate(parseDateFromString(terminalService.nextLine()));
        System.out.println("[OK]");
        taskService.merge(anotherTask);
    }
}
