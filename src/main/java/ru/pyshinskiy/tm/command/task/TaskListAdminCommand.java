package ru.pyshinskiy.tm.command.task;

import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.role.Role;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskListAdminCommand extends AbstractCommand {

    @Override
    public boolean isAllowed(final User user) {
        if(user == null) return false;
        return Role.ADMINISTRATOR.equals(user.getRole());
    }

    @Override
    public String command() {
        return "task_list_admin";
    }

    @Override
    public String description() {
        return "list all users tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST ADMIN]");
        printTasks(serviceLocator.getTaskService().findAll());
        System.out.println("[OK]");
    }
}
