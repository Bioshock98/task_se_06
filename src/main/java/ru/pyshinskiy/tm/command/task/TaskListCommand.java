package ru.pyshinskiy.tm.command.task;

import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskListCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task_list";
    }

    @Override
    public String description() {
        return "show all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST]");
        printTasks(serviceLocator.getTaskService().findAll(serviceLocator.getCurrentUser().getId()));
        System.out.println("[OK]");
    }
}
