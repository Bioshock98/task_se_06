package ru.pyshinskiy.tm.command.task;

import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.entity.User;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskAttachCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task_attach";
    }

    @Override
    public String description() {
        return "attach task to project";
    }

    @Override
    public void execute() throws Exception {
        final IProjectService projectService = serviceLocator.getProjectService();
        final ITaskService taskService = serviceLocator.getTaskService();
        final User currentUser = serviceLocator.getCurrentUser();
        System.out.println("[ATTACH TASK]");
        System.out.println("ENTER PROJECT ID");
        printProjects(projectService.findAll(currentUser.getId()));
        final String projectId = projectService.getIdByNumber(Integer.parseInt(terminalService.nextLine()) - 1);
        final Project project = projectService.findOne(currentUser.getId(), projectId);
        System.out.println("ENTER TASK ID");
        printTasks(taskService.findAll(serviceLocator.getCurrentUser().getId()));
        final String taskId = taskService.getIdByNumber(Integer.parseInt(terminalService.nextLine()) - 1);
        final Task task = taskService.findOne(currentUser.getId(), taskId);
        task.setProjectId(project.getId());
        taskService.merge(task);
        System.out.println("[OK]");
    }
}
