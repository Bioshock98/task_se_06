package ru.pyshinskiy.tm.command.task;

import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.entity.User;

import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskListByProjectCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task_list_by_project";
    }

    @Override
    public String description() {
        return "show tasks by project";
    }

    @Override
    public void execute() throws Exception {
        final IProjectService projectService = serviceLocator.getProjectService();
        final ITaskService taskService = serviceLocator.getTaskService();
        final User currentUser = serviceLocator.getCurrentUser();
        System.out.println("[TASK LIST BY PROJECT]");
        System.out.println("[ENTER PROJECT ID");
        printProjects(projectService.findAll(serviceLocator.getCurrentUser().getId()));
        final String projectId = projectService.getIdByNumber(Integer.parseInt(terminalService.nextLine()) - 1);
        final List<Task> tasksByProjectId = taskService.findAllByProjectId(currentUser.getId(), projectId);
        printTasks(tasksByProjectId);
        System.out.println("[OK]");
    }
}
