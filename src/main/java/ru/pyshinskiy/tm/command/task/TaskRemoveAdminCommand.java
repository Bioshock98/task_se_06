package ru.pyshinskiy.tm.command.task;

import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.role.Role;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskRemoveAdminCommand extends AbstractCommand {
    
    @Override
    public boolean isAllowed(final User user) {
        if(user == null) return false;
        return Role.ADMINISTRATOR.equals(user.getRole());
    }

    @Override
    public String command() {
        return "task_remove_admin";
    }

    @Override
    public String description() {
        return "remove any user task";
    }

    @Override
    public void execute() throws Exception {
        final ITaskService taskService = serviceLocator.getTaskService();
        System.out.println("[TASK REMOVE]");
        System.out.println("ENTER PROJECT ID");
        System.out.println("ENTER TASK ID");
        printTasks(taskService.findAll());
        final String taskId = taskService.getIdByNumber(Integer.parseInt(terminalService.nextLine()) - 1);
        taskService.remove(taskId);
        System.out.println("[TASK REMOVED]");
    }
}
