package ru.pyshinskiy.tm.command.task;

import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskRemoveCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task_remove";
    }

    @Override
    public String description() {
        return "remove task";
    }

    @Override
    public void execute() throws Exception {
        final ITaskService taskService = serviceLocator.getTaskService();
        final User currentUser = serviceLocator.getCurrentUser();
        System.out.println("[TASK REMOVE]");
        System.out.println("ENTER PROJECT ID");
        System.out.println("ENTER TASK ID");
        printTasks(taskService.findAll(serviceLocator.getCurrentUser().getId()));
        final String taskId = taskService.getIdByNumber(Integer.parseInt(terminalService.nextLine()) - 1);
        taskService.remove(currentUser.getId(), taskId);
        System.out.println("[TASK REMOVED]");
    }
}
