package ru.pyshinskiy.tm.command.task;

import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.role.Role;

import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskListByProjectAdminCommand extends AbstractCommand {
    
    @Override
    public boolean isAllowed(final User user) {
        if(user == null) return false;
        return Role.ADMINISTRATOR.equals(user.getRole());
    }

    @Override
    public String command() {
        return "task_list_by_project_admin";
    }

    @Override
    public String description() {
        return "list tasks by any user project";
    }

    @Override
    public void execute() throws Exception {
        final IProjectService projectService = serviceLocator.getProjectService();
        final ITaskService taskService = serviceLocator.getTaskService();
        final User currentUser = serviceLocator.getCurrentUser();
        System.out.println("[TASK LIST BY PROJECT]");
        System.out.println("[ENTER PROJECT ID");
        printProjects(projectService.findAll());
        final String projectId = projectService.getIdByNumber(Integer.parseInt(terminalService.nextLine()) - 1);
        final List<Task> tasksByProjectId = taskService.findAllByProjectId(currentUser.getId(), projectId);
        printTasks(tasksByProjectId);
        System.out.println("[OK]");
    }
}
