package ru.pyshinskiy.tm.command.task;

import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.entity.User;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateFromString;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskEditCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task_edit";
    }

    @Override
    public String description() {
        return "edit existing task";
    }

    @Override
    public void execute() throws Exception {
        final ITaskService taskService = serviceLocator.getTaskService();
        final User currentUser = serviceLocator.getCurrentUser();
        System.out.println("[TASK EDIT]");
        System.out.println("ENTER TASK ID");
        printTasks(taskService.findAll(currentUser.getId()));
        final String taskId = taskService.getIdByNumber(Integer.parseInt(terminalService.nextLine()) - 1);
        final Task task = taskService.findOne(currentUser.getId(), taskId);
        System.out.println("[ENTER TASK NAME]");
        final Task anotherTask = new Task();
        anotherTask.setName(terminalService.nextLine());
        anotherTask.setId(task.getId());
        System.out.println("ENTER TASK DESCRIPTION");
        anotherTask.setDescription(terminalService.nextLine());
        System.out.println("ENTER START DATE");
        anotherTask.setStartDate(parseDateFromString(terminalService.nextLine()));
        System.out.println("ENTER END DATE");
        anotherTask.setEndDate(parseDateFromString(terminalService.nextLine()));
        System.out.println("[OK]");
        taskService.merge(anotherTask);
    }
}
