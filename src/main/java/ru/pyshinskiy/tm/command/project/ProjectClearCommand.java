package ru.pyshinskiy.tm.command.project;

import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.User;

public final class ProjectClearCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project_clear";
    }

    @Override
    public String description() {
        return "remove all projects";
    }

    @Override
    public void execute() throws Exception {
        final IProjectService projectService = serviceLocator.getProjectService();
        final ITaskService taskService = serviceLocator.getTaskService();
        final User currentUser = serviceLocator.getCurrentUser();
        for(final Project project : projectService.findAll(currentUser.getId())) {
            taskService.removeAllByProjectId(serviceLocator.getCurrentUser().getId(), project.getId());
        }
    }
}
