package ru.pyshinskiy.tm.command.project;

import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.role.Role;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProject;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectSelectAdminCommand extends AbstractCommand {
    @Override
    public boolean isAllowed(final User user) {
        if(user == null) return false;
        return Role.ADMINISTRATOR.equals(user.getRole());
    }

    @Override
    public String command() {
        return "project_select_admin";
    }

    @Override
    public String description() {
        return "show any user project info";
    }

    @Override
    public void execute() throws Exception {
        final IProjectService projectService = serviceLocator.getProjectService();
        System.out.println("[PROJECT SELECT]");
        System.out.println("ENTER PROJECT ID");
        printProjects(projectService.findAll());
        final String projectId = projectService.getIdByNumber(Integer.parseInt(terminalService.nextLine()) - 1);
        final Project project = projectService.findOne(projectId);
        printProject(project);
        System.out.println("[OK]");
    }
}
