package ru.pyshinskiy.tm.command.project;

import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.role.Role;

public final class ProjectClearAdminCommand extends AbstractCommand {

    @Override
    public boolean isAllowed(final User user) {
        if(user == null) return false;
        return Role.ADMINISTRATOR.equals(user.getRole());
    }

    @Override
    public String command() {
        return "project_clear_admin";
    }

    @Override
    public String description() {
        return "remove all users projects";
    }

    @Override
    public void execute() throws Exception {
        final IProjectService projectService = serviceLocator.getProjectService();
        final ITaskService taskService = serviceLocator.getTaskService();
        for(final Task task : taskService.findAll()) {
            if(task.getProjectId() != null) taskService.remove(task.getId());
        }
        projectService.removeAll();
        System.out.println("[ALL PROJECTS REMOVED]");
    }
}
