package ru.pyshinskiy.tm.command.project;

import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.role.Role;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateFromString;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectEditAdminCommand extends AbstractCommand {
    
    @Override
    public boolean isAllowed(final User user) {
        if(user == null) return false;
        return Role.ADMINISTRATOR.equals(user.getRole());
    }

    @Override
    public String command() {
        return "project_edit_admin";
    }

    @Override
    public String description() {
        return "edit any project";
    }

    @Override
    public void execute() throws Exception {
        final IProjectService projectService = serviceLocator.getProjectService();
        System.out.println("[PROJECT EDIT]");
        System.out.println("ENTER PROJECT ID");
        printProjects(projectService.findAll());
        final String projectId = projectService.getIdByNumber(Integer.parseInt(terminalService.nextLine()) - 1);
        final Project project = projectService.findOne(projectId);
        final Project anotherProject = new Project(project.getUserId());
        System.out.println("ENTER NAME");
        anotherProject.setName(terminalService.nextLine());
        anotherProject.setId(project.getId());
        System.out.println("ENTER PROJECT DESCRIPTION");
        anotherProject.setDescription(terminalService.nextLine());
        System.out.println("ENTER START DATE");
        anotherProject.setStartDate(parseDateFromString(terminalService.nextLine()));
        System.out.println("ENTER END DATE");
        anotherProject.setEndDate(parseDateFromString(terminalService.nextLine()));
        projectService.merge(anotherProject);
        System.out.println("[OK]");
    }
}
