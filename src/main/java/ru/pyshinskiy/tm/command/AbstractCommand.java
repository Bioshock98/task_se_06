package ru.pyshinskiy.tm.command;

import ru.pyshinskiy.tm.api.service.ServiceLocator;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.service.TerminalService;

public abstract class AbstractCommand {


    protected ServiceLocator serviceLocator;

    protected TerminalService terminalService;

    public void setServiceLocator(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        this.terminalService = serviceLocator.getTerminalService();
    }

    public boolean isAllowed(final User user) {
        return user != null;
    }

    public abstract String command();

    public abstract String description();

    public abstract void execute() throws Exception;
}
