package ru.pyshinskiy.tm.command.help;

import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;

public final class HelpCommand extends AbstractCommand {

    @Override
    public boolean isAllowed(final User user) {
        return true;
    }

    @Override
    public String command() {
        return "help";
    }

    @Override
    public String description() {
        return "list available commands";
    }

    @Override
    public void execute() {
        for(final AbstractCommand command : serviceLocator.getCommands()) {
            if(command.isAllowed(serviceLocator.getCurrentUser())) {
                System.out.println(command.command() + ": " +
                        command.description());
            }
        }
    }
}
