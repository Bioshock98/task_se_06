package ru.pyshinskiy.tm.entity;

import java.util.Date;

public final class Task extends AbstractEntity {

    private String userId;
    private String name;
    private String projectId;
    private String description;
    private Date startDate;
    private Date endDate;

    public Task() {
    }

    public Task(final String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getProjectId() {
        return projectId;
    }

    public String setProjectId(final String projectId) {
        return projectId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(final Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(final Date endDate) {
        this.endDate = endDate;
    }
}
