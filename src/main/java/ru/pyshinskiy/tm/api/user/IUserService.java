package ru.pyshinskiy.tm.api.user;

import ru.pyshinskiy.tm.entity.User;

import java.util.List;

public interface IUserService {

    User findOne(final String id) throws Exception;

    List<User> findAll();

    User persist(final User user);

    User merge(final User user) throws Exception;

    User remove(final String id) throws Exception;

    void removeAll();

    String getIdByNumber(final int number);
}
