package ru.pyshinskiy.tm.api.service;

import java.util.List;

public interface Service<T> {

    T findOne(final String id) throws Exception;

    List<T> findAll();

    T persist(final T t);

    T merge(final T t) throws Exception;

    T remove(final String id) throws Exception;

    void removeAll();

    String getIdByNumber(final int number);
}
