package ru.pyshinskiy.tm.api.task;

import ru.pyshinskiy.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAll(final String userId);

    List<Task> findAllByProjectId(final String userId, final String projectId);

    Task findOne(final String id) throws Exception;

    Task persist(final Task task);

    Task merge(final Task merge) throws Exception;

    Task findOne(final String userId, final String id) throws Exception;

    Task remove(final String id) throws Exception;

    Task remove(final String userId, final String id) throws Exception;

    void removeAll(final String userId) throws Exception;

    void removeAllByProjectId(final String userId, final String projectId) throws Exception;

    void removeAll();

    String getIdByNumber(final int number);
}
