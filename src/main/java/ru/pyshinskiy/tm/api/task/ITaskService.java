package ru.pyshinskiy.tm.api.task;

import ru.pyshinskiy.tm.entity.Task;

import java.util.List;

public interface ITaskService {

    Task findOne(final String id) throws Exception;

    Task findOne(final String userId, final String id) throws Exception;

    List<Task> findAll();

    List<Task> findAll(final String userId);

    Task persist(final Task task);

    Task merge(final Task task) throws Exception;

    Task remove(final String id) throws Exception;

    Task remove(final String userId, final String id) throws Exception;

    void removeAll(final String userId) throws Exception;

    void removeAll();

    String getIdByNumber(final int number);

    List<Task> findAllByProjectId(final String userId, final String projectId);

    void removeAllByProjectId(final String userId, final String projectId) throws Exception;
}
