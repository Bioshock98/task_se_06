package ru.pyshinskiy.tm.bootstrap;

import org.reflections.Reflections;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.api.service.ServiceLocator;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.api.user.IUserService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.repository.ProjectRepository;
import ru.pyshinskiy.tm.repository.TaskRepository;
import ru.pyshinskiy.tm.repository.UserRepository;
import ru.pyshinskiy.tm.role.Role;
import ru.pyshinskiy.tm.service.ProjectService;
import ru.pyshinskiy.tm.service.TaskService;
import ru.pyshinskiy.tm.service.TerminalService;
import ru.pyshinskiy.tm.service.UserService;

import java.util.*;

public class Bootstrap implements ServiceLocator {

    private final TerminalService terminalService = new TerminalService();
    private final IProjectService projectService = new ProjectService(new ProjectRepository());
    private final ITaskService taskService = new TaskService(new TaskRepository());
    private final Map<String, AbstractCommand> commands = new TreeMap<>();
    private final IUserService userService = new UserService(new UserRepository());
    private User currentUser;

    @Override
    public TerminalService getTerminalService() {
        return terminalService;
    }

    @Override
    public User getCurrentUser() {
        return currentUser;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public void setCurrentUser(final User user) {
        this.currentUser = user;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public void start() throws Exception {
        createUsers();
        init();
        System.out.println("***WELCOME TO TASK MANAGER***");
        String command;
        while(true) {
            command = terminalService.nextLine();
            execute(command);
        }
    }

    private void init() throws Exception {
        final Reflections reflections = new Reflections("ru.pyshinskiy.tm.command");
        final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(ru.pyshinskiy.tm.command.AbstractCommand.class);
        for (final Class<? extends AbstractCommand> clazz : classes) {
            registry(clazz.newInstance());
        }
    }

    private void execute(final String command) throws Exception {
        if(command == null || command.isEmpty()) return;
        if("exit".equals(command)) System.exit(1);
        final AbstractCommand abstractCommand = commands.get(command);
        if(abstractCommand == null) {
            System.out.println("UNKNOW COMMAND");
            return;
        }
        if(!abstractCommand.isAllowed(getCurrentUser())) {
            System.out.println("UNAVAILABLE COMMAND");
            return;
        }
        abstractCommand.execute();
    }

    private void registry(final AbstractCommand command) throws Exception {
        final String cliCommand = command.command();
        final String cliDescription = command.description();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new Exception();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new Exception();
        command.setServiceLocator(this);
        commands.put(cliCommand, command);
    }

    private void createUsers() {
        final User user = new User(Role.USER);
        user.setLogin("user");
        user.setPassword("qwerty");
        final User admin = new User(Role.ADMINISTRATOR);
        admin.setLogin("admin");
        admin.setPassword("1234");
        userService.persist(user);
        userService.persist(admin);
        setCurrentUser(user);
    }
}
