package ru.pyshinskiy.tm.service;

import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.repository.AbstractRepository;
import ru.pyshinskiy.tm.repository.TaskRepository;

import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {
    
    private TaskRepository taskRepository = (TaskRepository) abstractRepository;

    public TaskService(final AbstractRepository<Task> abstractRepository) {
        super(abstractRepository);
    }

    @Override
    public Task findOne(final String userId, final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        if(userId == null || userId.isEmpty()) throw new Exception();
        return taskRepository.findOne(userId, id);
    }

    @Override
    public List<Task> findAll(final String userId) {
        if(userId == null) return null;
        return taskRepository.findAll(userId);
    }

    @Override
    public Task remove(final String userId, final String id) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        if(id == null || id.isEmpty()) throw new Exception();
        return taskRepository.remove(id);
    }

    @Override
    public void removeAll(final String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        taskRepository.remove(userId);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Override
    public void removeAllByProjectId(final String userId, final String projectId) throws Exception {
        if(projectId == null || projectId.isEmpty()) throw new Exception();
        taskRepository.removeAllByProjectId(userId, projectId);
    }
}
