package ru.pyshinskiy.tm.repository;

import ru.pyshinskiy.tm.api.repository.Repository;
import ru.pyshinskiy.tm.entity.AbstractEntity;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public abstract class AbstractRepository<T extends AbstractEntity> implements Repository<T> {

    protected Map<String, T> entityMap = new LinkedHashMap<>();

    @Override
    public List<T> findAll() {
        return new LinkedList<>(entityMap.values());
    }

    @Override
    public T findOne(final String id) throws Exception {
        return entityMap.get(id);
    }

    @Override
    public T persist(final T t) {
        return entityMap.put(t.getId(), t);
    }

    @Override
    public T merge(final T t) throws Exception {
        return entityMap.put(t.getId(), t);
    }

    @Override
    public T remove(final String id) throws Exception {
        return entityMap.remove(id);
    }

    @Override
    public void removeAll() {
        entityMap.clear();
    }

    @Override
    public String getIdByNumber(final int number) {
        final List<T> entities = findAll();
        for(final T t : findAll()) {
            if(t.getId().equals(entities.get(number).getId())) return t.getId();
        }
        return null;
    }
}
