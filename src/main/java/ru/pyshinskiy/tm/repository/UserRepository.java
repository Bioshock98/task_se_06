package ru.pyshinskiy.tm.repository;

import ru.pyshinskiy.tm.api.user.IUserRepository;
import ru.pyshinskiy.tm.entity.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {
}
